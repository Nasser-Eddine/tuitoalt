import re 
import pandas as pd
from verbecc import Conjugator, ConjugatorError
import itertools
    
############################# Pattern's Splits #############################

def split_pattern(pattern:str):
    '''
        split the pattern into different terms
    '''
    terms = []
    i = 0 
    #initial_len = len(pattern)
    while i < len(pattern):
        if pattern[i] == "[":
            # search for "]"
            while pattern[i] != "]" and i < len(pattern):
                i += 1
            terms.append(pattern[:i+1])
            pattern = pattern[i+1:].strip()
            i = 0 
        
        elif pattern[i] == "(":
            # search for "}"
            while pattern[i] != "}" and i < len(pattern):
                i += 1
            terms.append(pattern[:i+1])
            pattern = pattern[i+1:].strip()
            i = 0 
            
        elif pattern[i] == "#":
            # search for "#"
            i += 1
            while pattern[i] != "#" and i < len(pattern):
                i += 1
            terms.append(pattern[:i+1])
            pattern = pattern[i+1:].strip()
            i = 0 
    
        else:
            i += 1
    
    return terms

def term_label(term:str):
    '''
        return the label corresponding to each term 
        Labels : EVERY_OTHER_ONE, EVERY_OTHER_ONE, MANDATORY, PROBLEM
    '''
    if "[" in term: return "EVERY_OTHER_ONE"
    elif "(" in term: return "CONJUGATION"
    elif "#" in term: return "MANDATORY"
    else: return "PROBLEM"

'''Tag2Action''' 
def mandatory_split(term:str):
    '''
        
    '''
    term = term[1:-1]            # remove '#' at the beginning and at the end
    terms = term.split('|')
    return [t.strip() for t in terms]

def everyOtherOne_split(term:str):
    '''
        
    '''
    term = term[1:-1]            # remove '[' and ']' 
    terms = term.split('|') + [""]
    return [t.strip() for t in terms]

def conjugation_split(term:str):
    '''
        
    '''
    inverse = False

    pronouns = re.findall("\((.*?)\)", term)[0].split('|')
    verbs = re.findall("\((.*?)\)", term)[1].split('|')
    tenses = re.findall("\{(.*?)\}", term)[0].split('|')

    if "inverse" in term:
        inverse = True

    return [conjugation(v, t, p, inverse) for v in verbs for t in tenses for p in pronouns]


############################# Conjugation #############################

cg = Conjugator(lang='fr')

def conjugation(verb, tense, person, inverse=False):
    '''
        
    '''
    if tense == "présent": m, t = ("indicatif","présent")
    elif tense == "conditionnel": m, t = ("conditionnel","présent")
    elif tense == "impératif": m, t = ("imperatif","imperatif-présent")
    elif tense == "futur": m, t = ("indicatif","futur-simple")
    else: return "the tense is not valid"
    
    # convert person to the corresponding index 
    persons = {"je":0, "tu":1, "il":2, "nous":3, "vous":4, "ils":5}
    if tense == "impératif":
        persons = {"tu":0, "nous":1, "vous":2} 
    
    p = persons[person]

    try : 
        if inverse == False:
            return cg.conjugate(verb)["moods"][m][t][p]
        else:
            subject_verb = cg.conjugate(verb)["moods"][m][t][p]
            verb_subject = " ".join(subject_verb.split()[::-1])
            return verb_subject
        
    except ConjugatorError: 
        print(f"The configuration is not valid\n>> verb : {verb} | tense : {tense} | person : {person}")


############################# Rule & Variables #############################

def replace_vars_in_rule(variables:dict, rule:str):
    '''
        from a rule pattern and a dictionary of variables, generate a list of new utterances 
    '''
    # dictionary of variables 
    variables_dict = import_vars(variables)

    var_keys_list = list(variables_dict.keys())
    
    # generate new utterances 
    new_utt_list = [rule]
    while len(var_keys_list) != 0: 
        var_key = var_keys_list[0]      # take the first element of the var_keys_list
        tmp_list = []
        for pattern in new_utt_list: 
            for var_value in variables_dict[var_key]:
                # case of empty string 
                if var_value == '_':
                    var_value = ""
                # append the pattern to the temporary list 
                tmp_list.append(pattern.replace(var_key, var_value).strip())
        
        new_utt_list = tmp_list
        var_keys_list.remove(var_key)    # remove the var_key ($X) from the var_keys_list 

    return new_utt_list, variables_dict

def import_vars(variables:dict):
    '''
        creates a dictionary of variables from the excel column corresponding to variables (Var)
    '''
    # remove spaces at the extremities + replace '\n2028' by '\n'
    variables = str(variables).strip().replace('\u2028','\n')

    # + split using '\n' as the separator 
    variables_list = variables.split('\n')

    variables_dict = {}
    for var in variables_list:
        key = re.findall("^\$.", var)[0]
        key_vars = re.findall("\[.*.\]", var)[0]   # findall return a list -> take the first (unique) element
        key_vars = key_vars[1:-1]                  # remove brackets
        key_vars = key_vars.split(",")             # split
        key_vars = [kv.strip() for kv in key_vars] # remove spaces at the extremities
        
        # do not include key = $X 
        if "$X" not in key:
            variables_dict[key] = key_vars

    return variables_dict

def id2pattern(pattern_id, patterns_file):
    '''
    
    '''
    patterns = pd.read_csv(patterns_file, sep="\t").fillna('')
    return patterns[patterns['Pattern_Index']==pattern_id]['Pattern'].str.cat().strip()

def gp2id_list(pattern_ids):
    '''
    
    '''
    if str(pattern_ids) == "":
        return ""
    return [id_.strip() for id_ in str(pattern_ids).strip().split()] 


############################# Utterance Generation #############################

'''By Replacement'''

def new_utt_by_replacement_1Rule(utteranceCode, utteranceType, variables, rule, pattern_ids):
    '''
        example = ['UtteranceCode','UtteranceType','Var','Rule'].iloc[n]
        creates a dataframe of new utterances by replacing variables for ONE rule (synonymes and paraphrases)
    '''
    utterances, variables_dict = replace_vars_in_rule(variables, rule)
    return [{"Utterance":utt, 
             "UtteranceCode":utteranceCode, 
             "UtteranceType":utteranceType, 
             "Rule":rule,
             "Variables":variables_dict,
             "Pattern_IDs":pattern_ids} for utt in utterances]

def new_utt_by_replacement_allRules(df):
    '''
        creates a dataframe of new utterances by replacing variables for all rules (synonymes and paraphrases)
    '''
    data = []
    for _, example in df[['UtteranceCode','UtteranceType','Var','Rule']].iterrows():
        if sum(example.isnull()) == 0:
            data.extend(new_utt_by_replacement_1Rule(example))
        
    return pd.DataFrame(data, columns=['Utterance','UtteranceCode','UtteranceType'])


'''By Pattern'''

def new_utt_by_pattern(utterance, utteranceCode, utteranceType, generation_pattern, pattern_id):
    '''
    
    '''
    pattern = generation_pattern + "#" + utterance + "#"
    return [[newUtt, utteranceCode, utteranceType, pattern_id] for newUtt in newUttGeneration(pattern)]

def newUttGeneration(pattern):
    '''
    
    '''
    C = combinations_list(pattern)
    combs_list = list(itertools.product(*C))
    
    return [" ".join(c).strip() for c in combs_list]

def combinations_list(pattern):
    '''
        
    '''
    combs = []
    
    # split the pattern 
    terms = split_pattern(pattern)
    
    for term in terms: 
        # MANDATORY 
        if term_label(term) == "MANDATORY":
            combs.append(mandatory_split(term))
        elif term_label(term) == "EVERY_OTHER_ONE":
            combs.append(everyOtherOne_split(term))
        elif term_label(term) == "CONJUGATION":
            combs.append(conjugation_split(term))
        else: 
            print("Problem in the pattern")
    
    return combs

'''All Generations'''

def DataAugmentation(uttTable_file, patterns_file, date, export_emplacement_path):
    '''
    
    '''

    # import the table : [Utterance, UtteranceCode, UtteranceType, Rule, Var, Pattern_IDs] in a dataframe 
    df = pd.read_csv(uttTable_file, sep='\t')

    # replace all nan value by empty string 
    df.fillna("", inplace=True)
    
    # new utterances by replacement 
    newUtt_byReplacement = []
    for _, row in df.iterrows():
        # rule and variables exist 
        if row['Var'] != '' and row['Rule'] != '':
            newUtt_byReplacement.extend(
                new_utt_by_replacement_1Rule(row['UtteranceCode'],
                                             row['UtteranceType'],
                                             row['Var'],
                                             row['Rule'],
                                             row['Pattern_IDs'])
            ) 
        # without var and rule + original utterances 
        else:
            newUtt_byReplacement.append(
                {"Utterance":row['Utterance'].lower(), 
                 "UtteranceCode":row['UtteranceCode'], 
                 "UtteranceType":row['UtteranceType'],
                 "Rule":row['Rule'],
                 "Variables":row['Var'],
                 "Pattern_IDs":row['Pattern_IDs']}
            )
            
        
    # export new utterance by replacement dataframe 
    uttByReplacement_df = pd.DataFrame(newUtt_byReplacement)
    #to_csv
    
    
    ######################### new utterances by patterns ###################
    # get list of patterns 
    newUtt_byPattern = []
    for utt_BP in newUtt_byReplacement:
        patternIDs_list = gp2id_list(utt_BP['Pattern_IDs'])
        for pattern_id in patternIDs_list:
            generation_pattern = id2pattern(pattern_id, patterns_file)

            # assign values 
            utterance = utt_BP['Utterance'].lower()
            utteranceCode = utt_BP['UtteranceCode']
            utteranceType = utt_BP['UtteranceType']

            if pattern_id == "P5":
                # take the verb from the new utterance before modify the generation pattern
                verb = "("+utterance.split()[0]+")"
                # remove the verb from the utterance 
                utterance = " ".join(utterance.split()[1:])
                # replace '(v)' in the pattern by the corresponding verb
                generation_pattern = generation_pattern.replace('(v)',verb)
                
            # generate new utterance by pattern 
            newUtt_byPattern.extend(
                new_utt_by_pattern(utterance, 
                                   utteranceCode, 
                                   utteranceType, 
                                   generation_pattern,
                                   pattern_id)
            )
        

    augmented_data = pd.DataFrame(newUtt_byPattern, columns=['Utterance','UtteranceCode','UtteranceType','Pattern_ID'])

    # export the augmented data 
    augmented_data.to_csv(export_emplacement_path+"augmentedData_"+date+".tsv", sep="\t", index=False)

    
    # prints 
    print("\nAugmented Data : ")
    
    print(">>> Number of Utterances: ", augmented_data.shape[0])
    print(">>> Features : ", list(augmented_data.columns))
    
    print(">>> Pattern_IDs : ")
    values = augmented_data['Pattern_ID'].value_counts().keys().tolist()
    counts = augmented_data['Pattern_ID'].value_counts().tolist()
    nbr_patternIDs = dict(zip(values, counts))
    for pattern_ID, count in nbr_patternIDs.items():
        print(f"    * {pattern_ID} : {count}")
    
    print("Augmented Data has been successfully created")






    return augmented_data













    
    












