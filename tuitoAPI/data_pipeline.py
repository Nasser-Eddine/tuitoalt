import os
import json
import pandas as pd

############################### Data Pipeline ##############################

def DataPipeline(augmentedData_file, date, folders:dict, nbr_utt_per_file=20):
	'''
		Given the Augmented Data (generated outside this pipeline), 
		Generates : 
			* Interface Data 
			* ASR Data 
			* Intent Detection Data 
			* Intent Execution Data 
	'''
	# ------------------ interface data ---------------------
	interface_folder = folders['interface']
	InterfaceData(augmentedData_file, date, interface_folder, nbr_utt_per_file)


	# --------------------- asr data ------------------------
	asr_folder = folders['asr']
	ASRData(augmentedData_file, date, asr_folder)

	
	# ---------------- intent detection data ----------------
	intent_detection_folder = folders['intent_detection']
	IntentDetectionData(augmentedData_file, date, intent_detection_folder)


	# ---------------- intent execution data ----------------
	intent_execution_folder = folders['intent_execution']
	IntentExecutionData(augmentedData_file, date, intent_execution_folder) 


############################### InterfaceData #############################

def InterfaceData(augmentedData_file, date, folder_emplacement_path, nbr_utt_per_file=20):
	'''

	'''
	# import the augmentedData 
	augmentedData = pd.read_csv(augmentedData_file, sep="\t")

	# Local and Non-Local Data 
	local = augmentedData[augmentedData['UtteranceType']=="Local"]['Utterance']
	non_local = augmentedData[augmentedData['UtteranceType']!="Local"]['Utterance']

	# number of local and non-local utterances per file 
	nbr_local_utt_per_file = int(nbr_utt_per_file/2)
	nbr_non_local_utt_per_file = int(nbr_utt_per_file/2)
	
	final_nbr_non_local_utt = len(non_local)/nbr_utt_per_file
	non_local = non_local.sample(int(final_nbr_non_local_utt)*nbr_utt_per_file)

	# prints 
	print("\nInterface Data : ")
	print(">>> Number of Local Utterances: ", len(local))
	print(">>> Number of Non Local Utterances (Principal & Context) : ", int(final_nbr_non_local_utt)*nbr_utt_per_file)



	files = {}
	file_id = 0 
	while len(non_local) != 0: 
	    file_tmp = []
	    
	    # Local Utterance 
	    l_utt_tmp_id = list(local.sample(nbr_local_utt_per_file).index)
	    
	    for j in l_utt_tmp_id:
	        file_tmp.append({"id":str(j),"sentence":local[j],"project":"MIVOCO"})
	    
	    # Non-Local Utterance 
	    nl_utt_tmp_id = list(non_local.sample(nbr_non_local_utt_per_file).index)

	    for i in nl_utt_tmp_id:
	        file_tmp.append({"id":str(i),"sentence":non_local[i],"project":"MIVOCO"})
	        non_local = non_local.drop(i)
	    
	    # New file key  
	    files[f"data_{file_id}.js"] = {f"data_{file_id}.js":file_tmp}
	    file_id += 1  

	# Create a folder 
	folder_name = folder_emplacement_path+"InterfaceData_"+date
	if os.path.exists(folder_name) == False:
		# create the folder 
		os.mkdir(folder_name)

	# Create and Export Json files 
	for file_name, content in files.items():
		with open(f'{folder_name}/{file_name}', 'w') as f:
			json.dump(content , f)

	# Confirmation 
	print("InterfaceData has been successfully created")

################################# ASR Data #################################

def ASRData(augmentedData_file, date, folder_emplacement_path):
	'''

	'''
	

	# import the augmentedData 
	asrData = pd.read_csv(augmentedData_file, sep="\t")[['Utterance','UtteranceType']]

	# export 
	asrData.to_csv(folder_emplacement_path+"asrData_"+date+".tsv", sep="\t", index=False)

	# prints 
	print("\nASR Data : ")
	print(">>> Local Utt : ", asrData[asrData['UtteranceType']=='Local'].shape[0])
	print(">>> Context Utt : ", asrData[asrData['UtteranceType']=='Context'].shape[0])
	print(">>> Principal Utt : ", asrData[asrData['UtteranceType']=='Principal'].shape[0])
	print("ASRData has been successfully created")


########################### Intent Detection Data #########################

def IntentDetectionData(augmentedData_file, date, folder_emplacement_path):
	'''

	'''
	

	# import the augmentedData 
	intentDetectionData = pd.read_csv(augmentedData_file, sep="\t")[['Utterance','UtteranceCode','UtteranceType']]
	intentDetectionData = intentDetectionData[intentDetectionData['UtteranceType']=='Principal']

	# export 
	intentDetectionData.to_csv(folder_emplacement_path+"intentDetectionData_"+date+".tsv", sep="\t", index=False)

	# Number of each Utterance Code in a dictionary 
	values = intentDetectionData['UtteranceCode'].value_counts().keys().tolist()
	counts = intentDetectionData['UtteranceCode'].value_counts().tolist()
	nbr_UttCode = dict(zip(values, counts))

	# prints 
	print("\nIntentDetection Data : ")
	for uttCode, count in nbr_UttCode.items():
		print(f">>> {uttCode} : {count}")
	print("IntentDetection Data has been successfully created")


########################### Intent Execution Data ##########################

def IntentExecutionData(augmentedData_file, date, folder_emplacement_path):
	'''

	'''
	

	# import the augmentedData 
	intentExecutionData = list(set(pd.read_csv(augmentedData_file, sep="\t")['UtteranceCode']))

	# export 
	file_name = folder_emplacement_path + "intentExecutionData_" + date + ".txt"
	with open(file_name, 'w') as f:
		f.write('\n'.join(intentExecutionData))
	f.close()

	# prints 
	print("\nIntentExecution Data : ")
	print(">>> Number of UtteranceCode : ", len(intentExecutionData))
	print("nIntentExecution Data has been successfully created")

################################# Noisy Data ##############################

def NoisyData():
	'''

	'''
	pass 